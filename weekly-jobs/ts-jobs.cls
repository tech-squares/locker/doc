% ts-jobs.cls - style for the Tech Squares job descriptions
% Time-stamp: <2003-11-20 18:22:35 heather>
% $Id: ts-jobs.cls,v 1.2 2019/11/19 23:16:19 kchen Exp $
% By Stephen Gildea, Sept 2003

\NeedsTeXFormat{LaTeX2e}[1995/12/01]    %for \LoadClassWithOptions

\ProvidesClass{ts-jobs}[2003/09/17 Tech Squares Class job descriptions]
\LoadClassWithOptions{article}          %load our base class

\setlength{\headheight}{0pt}
\setlength{\headsep}{0pt}
\setlength{\topmargin}{.5in}
\setlength{\oddsidemargin}{.25in}
%\setlength{\textheight}{9.3in}
\setlength{\textwidth}{6.0in}

%% make itemize tighter
\renewcommand{\@listI}{\parsep 0pt \topsep 0pt \itemsep 0.5ex}
\renewcommand{\@listii}{\parsep 0pt \topsep 0pt \itemsep 0.5ex}
\newenvironment{itemize*}%
  {\begin{itemize}%
    \setlength{\itemsep}{0pt}%
    \setlength{\parskip}{0pt}}%
  {\end{itemize}}

%% no numbers on \section
\setcounter{secnumdepth}{0}

\RequirePackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}                      %no page numbers
\renewcommand{\headrulewidth}{0pt} %default is a headrule

\newcommand{\subtitle}[1]{\gdef\@subtitle{#1}}
\def\@subtitle{}

\renewcommand{\maketitle}{\par
  \begin{center}
    {\LARGE \@title \par}%
    {\Large \@subtitle \par}%
    {\tiny \@date \par}%
    \par%
  \end{center}%
}

\renewcommand{\section}{\@startsection {section}{1}{\z@}%
                                       {-1ex \@plus -.5ex \@minus -.2ex}%
                                       {1ex \@plus.2ex}%
                                       {\normalfont\large\bfseries}}

\renewcommand{\subsection}{\@startsection {subsection}{1}{\z@}%
                                       {-1ex \@plus -.5ex \@minus -.2ex}%
                                       {1ex \@plus.2ex}%
                                       {\normalfont\bfseries}}

%% time-stamp in the footer

% Define a LaTeX command that captures Emacs's time-stamp into something
% we can print at the bottom of the page.  Note the backslash in front
% of Time-stamp, which you normally don't need.
\def\Time-stamp: <#1 #2 #3>{\gdef\tmstamp{#1 #3}}

\setlength{\footskip}{0.2in}    %default of 30pt is too big
\def\tmstamp{}                  %set default if no time-stamp in manuscript
% The sigfont is intentionally not a proper 3pt font,
% which is why this looks complicated.  We want this to be
% compact; easy to read is not important.
\AtBeginDocument{\font\sigfont=\fontname\font\space at 3pt}
\rfoot{\sigfont \tmstamp}

% ts-jobs.cls ends here

