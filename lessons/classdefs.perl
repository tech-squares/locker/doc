# classdefs.perl - latex2html support for classdefs.cls
# for LaTeX2html 2002
# by Stephen Gildea
# $Id: classdefs.perl,v 1.4 2015/11/09 00:34:20 kchen Exp $
# 1998-03-02 previous version.
# 1999-02-17 changed subtitle "Spring 1998" to "Spring 1999"
# 1999-10-05 $max_handout_number
# 2002-11-15 contents.html support

package main;

# Here we note the number of the last handout, which has two lessons on it.
# All calls on both parts have to reference the same document.
# It's unfortunate that we have to know that here, but there
# isn't any way to get the information here from the document.
#
$max_handout_number = 0;
open(INDEXLOG, "< ../lesson1.ilg");
while (<INDEXLOG>) {
    /Scanning input file lesson([0-9]*)\.idx\.\.\./;
    $handout_number = $1;
    if ($handout_number > $max_handout_number) {
	$max_handout_number = $handout_number;
    }
}
close INDEXLOG;

$pdffilename = "$FILE.pdf";

$TOP_NAVIGATION = 0;
# 97.1 seems to ignore the variable, so we force the issue:
sub top_navigation_panel {}

$BOTTOM_NAVIGATION = 1;

$INFO = 0;

# PAPERSIZE determines the largest image we can converrt.
# Default is "a5", much smaller than our "formations" image.
$PAPERSIZE = "a3";


# override the default: no "meta description", add "link index".
sub meta_information {
    local($_) = @_;
    my ($first) = "";
    my ($previous) = "";
    my ($next) = "";
    my ($last) = "";
    my ($contents) = "";
    my ($contents_frag) = "";
    my ($index) = "";
    my ($fileweek) = $FILE;
    if (($fileweek =~ s/^lesson([0-9]+)$/$1/)) {
	if ($fileweek != 1) {
	    my ($prevweek) = $fileweek - 1;
	    $previous = "<link rel=\"Prev\" title=\"Week $prevweek.\" href=\"lesson$prevweek.html\">\n";
	}
	if ($fileweek != $max_handout_number) {
	    my ($nextweek) = $fileweek + 1;
	    $next = "<link rel=\"Next\" title=\"Week $nextweek.\" href=\"lesson$nextweek.html\">\n";
	}
	$contents_frag = "#lesson$fileweek";
    }
    if ($fileweek != 1) {
	$first = "<link rel=\"First\" title=\"First Week.\" href=\"lesson1.html\">\n";
    }
    if ($fileweek != $max_handout_number) {
	$last = "<link rel=\"Last\" title=\"Final Week.\" href=\"lesson$max_handout_number.html\">\n";
    }
    if ($FILE ne "lesson-index") {
	$index = "<link rel=\"Index\" title=\"Call Index.\" href=\"lesson-index.html\">\n";
    }
    if ($FILE ne "contents") {
	$contents = "<link rel=\"Contents\" title=\"Lessons Table of Contents.\" href=\"contents.html$contents_frag\">\n";
    }
    # style for Contents page:
    $style = "\n<style type=\"text/css\">\n" .
	"  h3 { margin-bottom: 0px; }\n" .
	"  p.list { margin-top: 0px; padding-left: 3em; }\n" .
	"</style>\n";

    # Output charset info because LaTeX2HTML v2002-2-1 fails to (but v2002 did)
    # Cannot have nested HTML tags...
    # "Top" is for Firefox's Link Toolbar extension.
    # "Home" is for Firefox's cmSiteNavigation extension, but alas it
    # also shows up in Link Toolbar's "Other related pages" folder, so
    # I'm not keeping it.
    #    "<link rel=\"Home\" title=\"Tech Squares.\" href=\"../\">\n" .
    do { s/<[^>]*>//g;
         "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=us-ascii\">\n" .
	 "<link rel=\"shortcut icon\" href=\"../tech-squares-logo-icon.png\">\n" .
	 "<link rel=\"alternate\" media=\"print\" title=\"This document in Acrobat\"\n" .
	 " type=\"application/pdf\" href=\"$pdffilename\">\n" .
	 $contents .
	 $first . $previous . $next . $last .
	 $index .
         "<link rel=\"Up\" title=\"Tech Squares Class Lessons.\" href=\"./\">\n" .
         "<link rel=\"Top\" title=\"Tech Squares.\" href=\"../\">\n" .
	 $style
     } if $_;
}

$BODYTEXT = "bgcolor=\"#ffffff\"";

# 176 is degree character in Latin-1 encoding
sub do_cmd_degr{
    join('',"&#176;",$_[0]);
}

sub do_cmd_weeknum {
    local($_) = @_;
    s/$next_pair_pr_rx//o;
    $subtitle = "Definitions for Week ".$&;
    # If more than one call to weeknum, the headers and footers should
    # not change.
    if (!$weeknum) {
	$weeknum = $&;
	($TITLE) = "Tech Squares Class: Definitions for Week ".$&;
    }
    $_;
}

sub do_cmd_subtitle {
    local($_) = @_;
    s/$next_pair_pr_rx//o;
    ($TITLE) = "Tech Squares Class: ".$&;
    $subtitle = $&;
    $_;
}

# \subtitle* suppresses the ToC entry in LaTeX, does nothing different here.
sub do_cmd_subtitlestar { &do_cmd_subtitle; }

# overrides the default definition
sub do_cmd_maketitle {
    join('', "<h1 align=\"center\">Tech Squares Class</h1>\n",
	 "<h2 align=\"center\">$subtitle</h2>\n<p>\n",
	 $_[0]);
}

# like call, but no colon
sub do_cmd_callmod{
    local($twoarg) = 1;		#dynamic!
    &callinternal;
}

sub do_cmd_call{
    local($twoarg) = 0;		#dynamic!
    &callinternal;
}

# <p><b>#1</b>:
sub callinternal{
    local($_) = @_;
    local($urlname) = &get_next_optional_argument;	#index entry
    local($rest) = $_;
    local($call);
    local($mod) = "";
    $rest =~ s/$next_pair_pr_rx//o;
    $call = $&;
    if ($urlname eq "") {
	$urlname = $call;
    } else {
	$urlname =~ s/[!@].*//;
    }
    if ($twoarg) {
	$rest =~ s/$next_pair_pr_rx//o;
	$mod = " ($&)";
    }
    $urlname =~ s/$OP\d+$CP//go; # get rid of latex2html ids before sanitize
    # sanitize name consistently with LaTeX hyperref
    $urlname =~ s/[{},\/() ]//ig;
    $urlname =~ s/[^-_.a-z0-9]/_/ig;
    $call =~ tr/a-z/A-Z/;
    join('',"<p><b><a name=\"$urlname\">$call<\/a><\/b>$mod:",$rest);
}

sub do_cmd_subheading{
    local($_) = @_;
    local($rest) = $_;
    $rest =~ s/$next_pair_pr_rx//o;
    my($heading) = $&;
    my($frag) = $heading;
    $frag =~ s/$OP\d+$CP//go; # get rid of latex2html ids before sanitize
    # sanitize name consistently with LaTeX hyperref
    $frag =~ s/[{},\/ ]//ig;
    $frag =~ s/[^-_.a-z0-9]/_/ig;
    join('',"<h3><a name=\"$frag\">",$heading,"</a><\/h3>",$rest);
}

# lesson-index support:

# want the index done normally
sub add_bbl_and_idx_dummy_commands {}

# we manually do our own index anchors
sub make_index_entry {
    local($br_id,$str) = @_;
    "<!--index$br_id-->";	#need to output something to suppress <P>
}

sub do_env_theindex {
    local($_) = @_;
    ## based on the regexps in &list_helper, but more special-purpose
    # items without a "page number"
    s/;[^;\n]+;([^,])/$1/g;
    s/\s*\\item$delimiter_rx(.*);(.*);,\s+(\d*)\n/\n<li><a href="lesson$4.html\#$3">$2<\/a>/g;
    s/\s*\\item$delimiter_rx(.*)\n/\n<li>$2/g;
    s/\s*\\subitem$delimiter_rx(.*);(.*);,\s+(\d*)\n/\n<ul><li><a href="lesson$4.html\#$3">$2<\/a><\/ul>/g;
    # previous line adds redundant end/start pairs; next line removes them
    s/<\/ul>\n<ul>/\n/g;
    # prettiness:
    s/<ul>([^\n])/<ul>\n$1/g;
    s/([^\n])<\/ul>/$1\n<\/ul>/g;
    s/<\/ul>([^\n])/<\/ul>\n$1/g;

    #Same sanitize on the fragment consistently with LaTeX hyperref.
    #And fix number of last lesson.
    @hrefs = split /<a href/;
    @newstrings = ();
    foreach $href (@hrefs) {
        ($frag) = $href =~ m/\"lesson[^.]*.html\#([^\"]*)\">/;
	$frag =~ s/[{},\/ ]//ig;
	$frag =~ s/[^-_.a-z0-9]/_/ig;
	($docnum) = $href =~ m/\"lesson([^.]*).html/;
	if ($docnum > $max_handout_number) { $docnum = $max_handout_number; }
	$href =~ s/lesson[^.]*\.html\#[^\"]*\"/lesson${docnum}.html\#${frag}\"/;
	push @newstrings, $href;
    }
    $_ = join '<a href', @newstrings;

    # output the whole thing
    "<ul>$_</ul>";
}

# Ignore the \small declaration in lesson-index.tex for the HTML version.
$declarations{small} = '';

sub do_env_multicols {
    local($_) = @_;
    local($ncols) = &get_next_argument;
    # do optional prefixed one-column text
    local($next, $pat) = &get_next_optional_argument;
    s/^\n//;		#ignore initial white space, still in "vertical mode"
    # We can put Table trappings around the multicol,
    # but too hard to figure out the breaks automatically.
    #$next . "<table><tr valign=top><td>" . $_ . "\n</td></tr></table>\n";
    # Or we can use the nicer but non-standard Netscape extention "multicol".
    # When CSS3 support for columns gets implemented in Mozilla,
    #  we'll test and use that.
    # CSA 2006-June: the ts-latex2html2web script does this for us now.
    $next . "<multicol cols=\"$ncols\">\n" 
	. "<div style=\"column-count: $ncols\">\n"
	. $_ . "\n</div>\n</multicol>\n";
}
$styles_loaded{multicol} = 1;	# we like ours better

# end of index code

# Table of Contents

$global_toc_started = 0;

$global_last_week_seen = 0;

sub do_cmd_contentsline {
    local($_) = @_;
    local($stype,$arg,$week,$frag);
    local($result, $after);
    # The form of the expression is:
    # \contentsline{SECTION} {TITLE}{WEEK}{HREF}
    $stype = &missing_braces unless (
        (s/$next_pair_pr_rx/$stype = $2;''/e)
        ||(s/$next_pair_rx/$stype = $2;''/e));
    $arg = &missing_braces unless (
        (s/$next_pair_pr_rx/$arg = $2;''/e)
        ||(s/$next_pair_rx/$arg = $2;''/e));
    $week = &missing_braces unless (
        (s/$next_pair_pr_rx/$week = $2;''/e)
        ||(s/$next_pair_rx/$week = $2;''/e));
    $frag = &missing_braces unless (
        (s/$next_pair_pr_rx/$frag = $2;''/e)
        ||(s/$next_pair_rx/$frag = $2;''/e));

    # hack to figure out when to close this
    if (s/^(\n$endfile_mark)/\n<\/p>$1/) {
	$global_toc_started = 0;
    }

    $after = $_;

    my($docnum) = $week;
    if ($docnum > $max_handout_number) { $docnum = $max_handout_number; }

    # sanitize on the fragment same as sub callinternal does.
    $frag =~ s/[{},\/() ]//ig;
    $frag =~ s/[^-_.a-z0-9]/_/ig;

    if ($stype eq "section") {
	if ($global_toc_started) {
	    $result .= "</p>\n";
	}
	if ($week > $global_last_week_seen) {
	    $frag = '';
	    $global_last_week_seen = $week;
	} else {
	    $frag = "#$frag";
	}
	$result .= "\n<h3>"
	    . "<a href=\"lesson$docnum.html$frag\" name=\"lesson$docnum\">" 
	    . $arg . "</a></h3>\n"
	    . "<p class=\"list\">";
	$global_toc_started = 1;
    }
    elsif ($stype eq "subsection") {
	$result .= "  <a href=\"lesson$docnum.html#$frag\">$arg</a><br>";
    }
    else {
	print "unhandled \\contentsline type {$stype}\n";
    }
    $result . $after;
}

# end of Table of Contents code

sub bot_navigation_panel {
    '<table><tr><td><a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a></td><td><small>&copy; 2015 Massachusetts Institute of Technology. <span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Tech Squares Class Handouts and Definitions</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://tech-squares.mit.edu/" property="cc:attributionName" rel="cc:attributionURL">Tech Squares</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.</td></tr></table>'.
    "<hr><p>\n".
    "Printable version: <a href=\"$pdffilename\"><tt>$pdffilename</tt></a>".
    '<br>'."\n".
    'Up: <a href="./">Lessons Table of Contents</a><br>'."\n".
    'Top: <a href="../">Tech Squares</a>'."\n"
}

sub make_address {
    "</body>\n</html>\n";
}

sub do_cmd_hrulefill {
    join('',"<hr>", $_[0]);
}

# "indexspace" is here because the blank lines around it do the job
&ignore_commands(<<_IGNORED_CMDS_);
Time
timestamp
sig
thepage
pagefooter # {}#{}#{}
leavevmode
voffset # &ignore_numeric_argument
NeedsTeXFormat #{}
titlevskip
documentlength
multicolsep
indexspace
_IGNORED_CMDS_

1;				# This must be the last line
