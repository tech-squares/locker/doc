A short history, by Stephen Gildea.

The definitions for the calls taught in the Tech Squares class were
first written up as handouts, I believe, by Peter Debin.

For the Spring 1995 class, I believe, I decided to redo them.  The
handouts we had didn't have the right calls in the right weeks, and I
didn't always like the wording of the definitions.
I did them in LaTeX, hoping to make the sources maintainable and to
allow generation of high-quality Acrobat and HTML.  The index page
was generated manually.

In Spring 1998, Ted took over from Don and re-did the teaching order.
I made a new set of handouts, this time generating the index automatically.
These handouts said "Spring 1998" at the top of each one.

Spring 1999: Ted made some small adjustments to the teaching order,
and I add Sweep a Quarter, which somehow didn't make it onto last
year's set in the right week.  Now say "Spring 1999" at the top.

Specific changes made Spring 1999:
-Wheel and Deal from all positions in week 1 (2FL was week 3).
-Left Square Thru no longer separate on week 4.  Both types taught week 2.
-Single Hinge moved to week 3 from week 5.
-Left Hand Wave not taught separately, removed from week 4.
-Reverse Flutterwheel added to call name of Flutterwheel on week 5.
-Sweep a Quarter added to week 8.  (Was stuck in at week 12.)

Fall 1999: class shortened from 14 to 13 weeks, and no separate fun night.
(Doing 13 weeks lets us graduate sooner even though we skip Thanksgiving.)
Heather worked with Ted to redo the teaching order.
We've had a few inquiries about using our handouts from other clubs,
so I added a copyright notice at the bottom.
At Heather's request, created a table of contents.

Specific changes made Fall 1999:
-Reverse Promenade no longer separate on week 3; combined onto week 1.
-Walk and Dodge moved from week 5 to week 8.
-Scoot Back moved from week 8 to week 5.
-Teacup Chain moved from week 9 to week 12.
-Spin Chain the Gears moved from week 10 to week 9.
-Diamond Circulate and Flip the Diamond moved from week 12 to week 9.
-Relay the Deucey moved from week 11 to week 10.
-(Anything) and Spread moved from week 10 to week 12.
-(Anything) and Roll moved from week 12 to week 10.
-Spin Chain and Exchange the Gears moved from week 13 to week 11.
-All 8 Spin the Top and Cut the Diamond moved from week 13 to week 12.
-Swing Your Partner moved from week 1 to week 2.

Fall 2003: Ted has reordered the calls somewhat to fit into 2 1/2
hours over 14 weeks instead of 3 hours over 13 weeks, consolidated
related calls, and "front-loaded" the teaching.

Specific a priori changes made Fall 2003:
-U Turn Back moved from week 2 to week 3
-Partner Trade moved from week 2 to week 1 
-Chain Down the Line moved from week 3 to week 2
-Dive Thru moved from week 3 to week 2
-Circulate moved from week 4 to week 3
-Couples Trade moved from week 4 to week 1
-Left Swing Thru moved from week 4 to week 3
-Single File Circulate moved from week 5 to week 3
-Touch 1/4 moved from week 5 to week 3
-Eight Chain Thru moved from week 6 to week 7
-Trade By moved from week 6 to week 4
-Turn Thru moved from week 6 to week 5
-Cast Off Three Quarters moved from week 7 to week 6 
-Centers In moved from week 7 to week 8
-Sweep a Quarter moved from week 8 to week 5
-Wrong Way Thar moved from week 8 to week 4
-Extend moved from week 9 to week 5 (and removed Extend from 1/4 tag)
-Ping Pong Circulate moved from week 9 to week 14 
-Coordinate moved from week 10 to week 11 
-Grand Swing Thru moved from week 10 to week 14
-Single Circle to a Wave moved from week 10 to week 13 
-(Anything) and Roll moved from week 10 to week 11
-Chase Right moved from week 11 to week 12 
-Peel the Top moved from week 11 to week 12
-3/4 Tag the Line moved from week 11 to week 8
-All 8 Spin the Top moved from week 12 to week 13
-Crossfire moved from week 12 to week 9
-Cut the Diamond moved from week 12 to week 13
-Teacup Chain moved from week 12 to week 13
-Dixie Grand moved from week 13 to week 14 

Promenade, Partner Trade, and Four Ladies Chain edited to fit 
Lesson One on two sheets.

Changes made by Ted during the Fall 2003 class as we went:
-After Class 1: Lead Right/Left moved from Lesson 1 to Lesson 2
-After Class 2: Dive Thru forgotten and moved back to week 3
	        (Swing Thru and Left Swing Thru consolidated to make room)

Spin Chain the Gears moved from week 9 to week 11,
Spin Chain and Exchange the Gears moved from week 11 to week 12


Changes made Spring 2004 to make the class fit into 13 weeks:
Spin Chain the Gears moved from week 11 to week 9
All 8 Spin the Top moved from week 13 to week 10
Spin Chain and Exchange the Gears moved from week 12 to week 11
Cut the Diamond moved from week 13 to week 11
Teacup Chain moved from week 13 to week 12
all of week 14 moved to week 13

March 16, 2004 we cancelled due to snow.  Then on March 30 (week 6) we
had elections, and Ted taught a bunch of extra calls during elections,
getting mostly caught up from the snow day.  Before week 7, we
rearranged the remaining calls to fit into 12 weeks, essentially
cancelling week 7.  We'll revert all these changes for Fall 2004.

Ran this script (remove-lesson-7.sh):
mv -i lesson7.tex lesson7-save.tex
mv -i lesson8.tex lesson7.tex
mv -i lesson9.tex lesson8.tex
mv -i lesson10.tex lesson9.tex
mv -i lesson11.tex lesson10.tex
mv -i lesson12.tex lesson11.tex
mv -i lesson13.tex lesson12.tex

... then made these additional changes:
lesson9 (was 10): 
  move Relay the Deucey from 9 to 10,
  move Anything and Roll here from week 10 to week 9,
  move Spin Chain the Gears from 8 to 9
lesson10 (was 11): \documentlength{2}
 move Spin Chain and Exchange the Gears from 10 to 11
 move Track II from 11 to 10
lesson11 (was 12): 
 move Ping Pong Circulate to 11 from 12
lesson 12 (was 13):
lesson 6: \documentlength{8}
  move from 7 to 6: Alamo Style Wave, Balance, Cross Fold, Eight Chain Thru, Fold
lesson 7 (was 8): \documentlength{7}, 
  keep Circle to a Line, Slide Thru, Spin the Top
  move Fan the Top from 7 to 8
  move Spin Chain Thru from 7 to 8
lesson 8 (was 9): \documentlength{1}

These changes were forgotten and not reverted until Spring 2006
(5 Feb 2006).  At that time I changed the date at the bottom to
"Spring 2006" even though there were no changes, just to avoid
confusion with the last 18 months.

Fall 2005 temporary changes:  Moved Track II from week 10 to week 11.
Moved Peel the Top, Ping Pong Circulate, Spin Chain and Exchange the
Gears, and Anything and Spread from week 11 to week 12.

March 2006: Clark says:
You use the term "rollaway with a  half sashay" on several pages at
   this site.  The proper term is now "rollaway".
See http://www.callerlab.org/programs/mslist.asp
This change was made in September 2001.
In the past, "Rollaway" has also been called "Rollaway With A
   Half Sashay".  In an effort to increase the auditory distance
   between "Half Sashay" and "Rollaway", callers are requested to call
   "Rollaway" in place of "Rollaway With A Half Sashay".
