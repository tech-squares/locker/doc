Hello.  This is the doc/ folder, used to store various documents relating to
the club:

  c4: Papers written on various concepts in challenge square dancing.

  call-lists: lists of the calls on various dance programs.  These lists
     are highly likely to be out-of-date (sigh).

  callerlab-definitions: the official definitions of the calls on
     callerlab's dance programs.  Like call-lists, these change from
     year-to-year, and thus are likely to be out-dated.  Check
     callerlab.org for updates.

  class: various forms and bits of paper useful for club Class
     Coordinators.

  club: the club bylaws, constitution, and other documents relating to
     club business.

  lessons: the source files for the class handouts.

  logo: the Tech Squares Logo

  off-hand: the Tech Squares officers' handbook.

  sd: some (out-dated) documentation for the sd square dance
     choreography program.

  weekly-jobs: how to perform the various jobs of the club.

Many of the files here (and the files comprising the web site) are
managed with a program called CVS (and earlier, historically, with
RCS).  CVS saves information about the edits to a file and allows us
to see how a file has changed over time, who has made those changes,
and why.

CVS is in the gnu locker.  You may need to 'add gnu'.

If you use emacs, using CVS is very simple: after you have changed any
file managed by CVS (and emacs puts 'CVS' in the bottom status line of
the window to let you know this is the case), use 'C-x v v' to check
in your changes.  (That's "control-x" and then press v twice.)
Emacs will open a window asking you to write a brief comment on your
edits (eg, "added information about June 17 dance" or "moved spin
chain thru to week 5 of the class"), and then you press "C-c C-c" to
"commit" (ie, record) your changes with this log message.  (Again,
that's "control-c" twice.)  That's all!

If emacs complains "file not found <something something> cvs", then go
back and 'add gnu', then reopen emacs and try again.

If you're not using emacs, then after you are done editing a file just
remember to run:

% cvs commit -m "<your log message>" <filenames>

from the command line, giving a brief description of the changes made
and then listing all the files you have changed.  (Again, if you get
"cvs: command not found" back from the shell, then 'add gnu' and try
again.)
                     -- C. Scott Ananian, cananian@alum, 22-Jun-2006
